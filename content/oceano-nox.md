---
title: "oceano nox"
date: 2024-05-24T23:26:15-03:00
---
A escuma e as vagas rebentam-se assaz,  
Em rochas, penedos, recifes sem fim.  
Também os navios se perdem assim:  
De Tróia vieram, sem trégua, sem paz.  
(O mar, como a noite, a morte lhes traz…)  
Embora guiados por ti que és sem par,  
Enéias, teus homens não podes salvar!  
Os vejo espantados, sem fé, sem poder,  
Porém nada eu posso lhes oferecer,  
Senão um galope na beira do mar.  
